
# README #

### What is this repository for? ###

* Quick summary

This repository holds Allan's solution for the GSNT Staff Architect coding exercise.

* Version 1.0.

### How do I get set up? ###

* Summary of set up

This is a set of two MEAN applications and one Node app that mashes both MEAN apps up into a portal.

I'm happy to walk through my code and do a demo at any time - especially if the instructions below don't work for you.

1. Install MongoDB and Node.js
2. Download the contents of this repository under your C:\ drive (as in "C:\nodeprojects") or suitable location.
3. Start your Mongo instance and use transfer_list, e.g.:
	cd /c/mongo/bin
	./mongod

	and in another window ...

	cd /c/mongo/bin
	./mongo
	use transfer_list

4. Start up the first web app: Automated Dial Transfer 1.

	a. cd /c/nodeprojects/adtapp
	b. node server
	c. Navigate to http://localhost:3000

5. Start up the second web app: Automated Dial Transfer 2.

	a. cd /c/nodeprojects/adt2app
	b. node server
	c. Navigate to http://localhost:3001

6. Start up the Portal app.

	a. cd /c/nodeprojects/portalapp
	b. node server
	c. Navigate to http://localhost:1000

* Configuration

The web apps are MEAN apps.  The Portal app is a Node app. (It has no Angular, Express or Mongo.)

* Dependencies

You'll need Node.js and MongoDB installed.

* Database configuration

One MongoDB database and collection named "transfer_list" is required.
Data pre-population is Not required.

* How to run tests

Once all apps are running (see Configuration, above), test each Automated Dial Transfer Hub application separately.
For example, enter some values and hit "Add Transfer". You'll see that a dial transfer record is stored.
You can also remove dial transfer records, and edit/update existing records.

The two Automated Dial Transfer apps work the same way. Aside from the fact that they are near-identical copies
of each other running as separate MEAN apps on different ports, the only difference between them is that the 
"Automated Dial Transfer Too" app doesn't accept or display the "Transfer Reason" field.  This difference was
introduced to demonstrate that the two apps are in fact different apps and can surface different data and functionality.

The Portal app is a simple mashup of the two web apps.  Add at least one dial transfer record using one of the 
standalone apps or using an iFrame in the portal app, and then you'll see the record in the mashup. Copy and paste
one of the Mongo _id values from one of the iFrames into the text input box at the top of the portal and hit Submit.
You'll see that the Portal executed a function within each iFrame to bring the data values corresponding to that _id
into the input boxes of each iFrame-hosted web app. (That demonstrates portal-to-app interaction.)

* Deployment instructions

See Configuration, above.

### Contribution guidelines ###

Chat with me if you want to contribute.

### Who do I talk to? ###

Allan.C.Pomeroy@gmail.com