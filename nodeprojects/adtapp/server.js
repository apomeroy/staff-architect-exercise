
// Enable use of Express framework for Node
var express = require('express');
var app = express();

// Use Mongo
var mongojs = require('mongojs');

// Identify the mongo database & collection we'll be using
var db = mongojs('transfer_list', ['transfer_list']);

// Allow node to parse the contents of POST requests
var bodyParser = require('body-parser');

// Tell node to get static content from the "public" subdirectory
app.use(express.static(__dirname + "/public"));

// Allow our code to parse the content of POST requests through the magic of the node component body-parser
app.use(bodyParser.json());

// Define a RESTful GET request on the /transfers route for getting transfers
app.get('/transfers', function (req, res) {
	console.log("I received a GET request");

	// Retrieve the data in transfers into the docs object
	// and return the docs object to the controller as a JSON object.
	db.transfer_list.find(function (err, docs) {
		console.log(docs);
		// Send the data back to the controller
		res.json(docs);
	});
}); // End of app.get

// Define a RESTful POST request on the /transfers route to add a new transfer to the transfers
app.post('/transfers', function (req, res) {
	console.log(req.body);

	// Insert the new transfer object into the mongo db.
	db.transfer_list.insert(	req.body, 
								function (err, doc) {
									// Also return the doc to the client as a JSON object.
									res.json(doc);
								}
							);
}); // End of app.post

// Define a RESTful DELETE request on the /transfers route to delete a transfer from the list
app.delete('/transfers/:id', function (req, res) {
	var id = req.params.id;
	console.log("about to delete record with id = " + id + ".");

	// Delete the specified transfer object from the mongo db.

	db.transfer_list.remove(	{	_id: mongojs.ObjectId(id)
								}, function (err, doc) {
									res.json(doc);
								}
						   );
}); // End of app.delete

// Respond to a GET request (by ID) for a specific item. 
// Send the data for the transfer back to the controller.

app.get('/transfers/:id', function (req, res) {
	var id = req.params.id;
	console.log("I received a GET request for ID " + id);

	// Retrieve the specific transfer object from Mongo
	// and return it as a JSON object to the client controller.
	db.transfer_list.findOne(	{	_id: mongojs.ObjectID(id)
								},	function (err, doc) {
									res.json(doc);
								}
							);
}); // End of app.get by ID

// Handle to a PUT request to update a transfer object.

app.put('/transfers/:id', function (req, res) {
	var id = req.params.id;
	console.log("I received an update request for account " + req.body.account);

	// Update the record in Mongo.
	db.transfer_list.findAndModify	(	{	query: 	{	_id: mongojs.ObjectId(id)
													},
											update: {	$set: 	{	account: req.body.account,
																	customer_name: req.body.customer_name,
																	transfer_reason: req.body.transfer_reason
																}
													}, new: true
										}, function (err, doc) 	{	res.json(doc);
																}
									);
}); // End of app.put

// Start listening for requests
app.listen(3000);
console.log('Server running on port 3000');

