// This runs in the browser.

var startTime = 0;
var endTime = 0;

var myApp = angular.module('myApp',[]);

myApp.controller('AppCtrl', ['$scope', '$http',
function($scope, $http) 
	{ // Beginning of the container function

		// Reusable logic to record and display response times

		var timeStart = function() {
			startTime = (new Date()).getTime();
		}
		var timeEnd = function () {
			endTime = (new Date()).getTime();
			responseTime = (Number(endTime) - Number (startTime));

			document.getElementById('responseTimeDisplay').value = responseTime;
		}

		// Reusable logic to refresh the screen

		var refresh = function() {
			// Get the transfer list from the server using a RESTful GET request
			$http.get('/transfers').success(function(response) {
				console.log("I got the data I requested");
				$scope.transfers = response;
				$scope.transfer = "";
			});
		}

		// Refresh the screen
		timeStart();
		refresh();
		timeEnd();

		// Define a POST request to send a new transfer object to the server
		$scope.addTransfer = function () {
			timeStart();
			console.log($scope.transfer);

			// Now send the transfer object to the server
			$http.post('/transfers', $scope.transfer)
				.success (	function (response) {
								console.log(response);
								// Refresh the view so the newly-inserted transfer object appears in the list.
								refresh(); 
							}
						 );
			timeEnd();
		};

		// Define a POST request to ask the server to delete a transfer from the list
		$scope.removeTransfer = function (id) {
			timeStart();
			console.log("About to remove transfer with ID " + id);

			// Sent a RESTful DELETE request to effect the deletion
			$http.delete('/transfers/' + id);
			// Refresh the screen to show that the removal was successful
			refresh();
			timeEnd();
		}

		// Define edit functionality to retrieve a specific transfer and put it into the input fields
		$scope.editTransfer = function (id) {
			timeStart();
			console.log("ADT1: About to edit transfer with ID " + id);

			// Send a GET request to get (by id) the latest values for this transfer object
			// and put the retrieved values into the input boxes
			$http.get('/transfers/' + id).success(function(response) {
				$scope.transfer = response;
			});
			timeEnd();
		}

		// Define Update functionality to update the values for the currently-selected transfer
		$scope.updateTransfer = function () {
			timeStart();
			console.log("About to update transfer with id = " + $scope.transfer._id);

			// Use a PUT to send the data to the server be updated.
			$http.put('/transfers/' + $scope.transfer._id, $scope.transfer)
				.success 	(	function(response)	{	refresh() // To show the updated value(s) on the screen
													}
							);
			timeEnd();
		}

		// Implement a function to clear the input fields upon the user's request
		$scope.clearInputs = function () {
			timeStart();
			$scope.transfer = "";
			timeEnd();
		}

} // End of the container function

]);
